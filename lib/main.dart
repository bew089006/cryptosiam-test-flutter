import 'package:flutter/material.dart';
import 'shared/avatar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'contants.dart';
import 'shared/share_button.dart';
import 'components/holdings.dart';
import 'components/content_container.dart';
import 'components/transaction_row.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  double money = 99999;
  double returnMoney = 999;

  double ror(money, returnMoney) {
    return (returnMoney * 100) / money;
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cryptosiam test',
      theme: ThemeData(),
      home: Scaffold(
        backgroundColor: Color(0xffF3F4F6),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            color: Color(0xffF3F4F6),
            height: 50,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                bottomButton(FontAwesomeIcons.home, () {
                  print("Click Home");
                }, Color(0xff8E9498), "Home"),
                bottomButton(FontAwesomeIcons.signal, () {
                  print("Click Prices");
                }, Color(0xff8E9498), "Prices"),
                bottomButton(FontAwesomeIcons.exchangeAlt, () {
                  print("Click Payments");
                }, Color(0xff8E9498), "Payments"),
                bottomButton(FontAwesomeIcons.globe, () {
                  print("Click News");
                }, Color(0xff8E9498), "News"),
                bottomButton(FontAwesomeIcons.heart, () {
                  print("Click Invite");
                }, Color(0xff8E9498), "Invite"),
              ],
            ),
          ),
        ),
        body: SafeArea(
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Avatar(
                            imgPath: "assets/images/jennie_0.jpg",
                            height: 70,
                            width: 70,
                            circularWeight: 50,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: Icon(
                              FontAwesomeIcons.bell,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Balance",
                        style: kTitleStyle(),
                      ),
                      Text(
                        "฿${widget.money.toStringAsFixed(2)}",
                        style: TextStyle(
                          fontSize: 36,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "+฿${widget.returnMoney.toStringAsFixed(2)} (+${widget.ror(widget.money, widget.returnMoney).toStringAsFixed(2)})",
                            style: TextStyle(
                              color: Color(0xff36c37d),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Today",
                            style: TextStyle(
                              color: Colors.black45,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 10,
                            child: ShareButton(
                              iconData: FontAwesomeIcons.plus,
                              title: "Add Money",
                              onPress: () {
                                print("Add money");
                              },
                            ),
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Expanded(
                            flex: 10,
                            child: ShareButton(
                              iconData: FontAwesomeIcons.arrowRight,
                              title: "Send",
                              onPress: () {
                                print("send");
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      ContentContainer(
                        body: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Holdings(
                              groupName: "Fiat Holdings",
                              holdingData: fiatHoldingData,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Holdings(
                              groupName: "Crypto Holdings",
                              holdingData: bitcoinHoldingData,
                              showUnit: true,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Recent Transaction",
                            style: kTitleStyle(),
                          ),
                          Text(
                            "See All",
                            style: TextStyle(
                              color: Color(0xff519BF8),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      ContentContainer(
                        body: Column(
                          children: [
                            for (var transaction in transactionData)
                              TransactionRow(
                                titleName: transaction['title_name'],
                                time: transaction['time'],
                                desc: transaction['desc'],
                                imgPath: transaction['profileImage'],
                                price: transaction['price'],
                                unit: transaction['unit'],
                                onPress: () {
                                  print(
                                      "Click at ${transaction['title_name']}");
                                },
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

List<Map> fiatHoldingData = [
  {
    "name": "China yuan",
    "currency": "YUAN",
    "price": 39000.00,
    "logoPath": "assets/images/holding_logo/china.png"
  },
  {
    "name": "Thai Baht",
    "currency": "THB",
    "price": 30000.00,
    "logoPath": "assets/images/holding_logo/thai.png"
  },
];

List<Map> bitcoinHoldingData = [
  {
    "name": "Bitcoin",
    "currency": "BTC",
    "price": 600000.00,
    "logoPath": "assets/images/holding_logo/bitcoin.png",
    "unit": 0.6999,
  },
  {
    "name": "Ethereum",
    "currency": "ETH",
    "price": 20000.00,
    "logoPath": "assets/images/holding_logo/etherium.png",
    "unit": 0.6999,
  },
  {
    "name": "Dogecoin",
    "currency": "DOGE",
    "price": 0.06,
    "logoPath": "assets/images/holding_logo/dogcoin.png",
    "unit": 0.6999,
  },
];

List<Map> transactionData = [
  {
    "title_name": "To jennie",
    "time": "Today 3:30pm",
    "desc": "to lunch",
    "profileImage": "assets/images/jisoo.jpg",
    "price": 288.59,
    "unit": 0.005
  },
  {
    "title_name": "To rose",
    "time": "Yesterday 3:30pm",
    "desc": "to lunch",
    "profileImage": "assets/images/lisa.jpg",
    "price": 288.59,
    "unit": 0.005
  },
  {
    "title_name": "To jisoo",
    "time": "Monday 02/03/2021 3:30pm",
    "desc": "to lunch",
    "profileImage": "assets/images/rose.jpg",
    "price": 288.59,
    "unit": 0.005
  },
];

Widget bottomButton(
    IconData iconData, Function onPress, Color color, String title) {
  return GestureDetector(
    onTap: onPress,
    child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Icon(
        iconData,
        color: color,
        size: 20,
      ),
      Text(title),
    ]),
  );
}
