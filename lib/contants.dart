import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle kTitleStyle() {
  return TextStyle(color: Colors.black45);
}

TextStyle kMoneyStyle() {
  return TextStyle(
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );
}
