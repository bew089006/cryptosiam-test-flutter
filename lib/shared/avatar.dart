import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final double width;
  final double height;
  final String imgPath;
  final double circularWeight;
  final Function onPress;
  final bool isNetwork;

  Avatar(
      {@required this.width,
      @required this.height,
      this.imgPath,
      this.isNetwork = false,
      this.circularWeight = 50,
      this.onPress});

  @override
  Widget build(BuildContext context) {
    Image img;
    if (imgPath != null) {
      if (isNetwork) {
        img = Image.network(
          imgPath,
          fit: BoxFit.cover,
        );
      } else {
        img = Image.asset(
          imgPath,
          fit: BoxFit.cover,
        );
      }
    }

    return GestureDetector(
      onTap: onPress,
      child: Container(
        width: width,
        height: height,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(circularWeight),
          child: Container(
            child: imgPath != null
                ? img
                : SizedBox(
                    width: width,
                    height: height,
                    child: Container(
                      color: Colors.white54,
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
