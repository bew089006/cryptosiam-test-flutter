import 'package:flutter/material.dart';

class ShareButton extends StatelessWidget {
  final String title;
  final IconData iconData;
  final Function onPress;

  ShareButton({@required this.title, this.iconData, this.onPress});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      color: Color(0xffDEEAF6),
      onPressed: onPress,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            iconData,
            color: Color(0xff519BF8),
            size: 15,
          ),
          SizedBox(width: 10),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Color(0xff519BF8),
            ),
          ),
        ],
      ),
    );
  }
}
