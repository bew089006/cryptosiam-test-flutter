import 'package:flutter/material.dart';
import '../contants.dart';
import 'holding_row.dart';

class Holdings extends StatelessWidget {
  final List holdingData;
  final String groupName;
  final bool showUnit;

  Holdings({@required this.groupName, this.holdingData, this.showUnit = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(groupName, style: kTitleStyle()),
        SizedBox(
          height: 10,
        ),
        Column(
          children: [
            for (var holding in holdingData != null ? holdingData : [])
              HoldingRow(
                showUnit: showUnit,
                name: holding["name"],
                currency: holding["currency"],
                logo: holding['logoPath'],
                price: holding['price'],
                unit: (showUnit ? holding['unit'] : null),
                onPress: () {
                  print("Click at ${holding["name"]}");
                },
              ),
          ],
        ),
      ],
    );
  }
}
