import 'package:cryptosiam_test_flutter/contants.dart';
import 'package:flutter/material.dart';
import '../shared/avatar.dart';

class HoldingRow extends StatelessWidget {
  final String logo;
  final String name;
  final String currency;
  final double price;
  final double unit;
  final bool showUnit;
  final Function onPress;

  HoldingRow(
      {this.logo,
      this.name,
      this.currency,
      this.price,
      this.unit,
      this.showUnit,
      this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Avatar(
                imgPath: logo,
                width: 50,
                height: 50,
                circularWeight: 50,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: kMoneyStyle(),
                  ),
                  Text(
                    currency,
                    style: kTitleStyle(),
                  ),
                ],
              ),
            ),
            Spacer(flex: 4),
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "฿${price.toStringAsFixed(2)}",
                    style: kMoneyStyle(),
                  ),
                  showUnit
                      ? Text(
                          "฿${unit.toStringAsFixed(2)}",
                          style: kTitleStyle(),
                        )
                      : SizedBox(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
