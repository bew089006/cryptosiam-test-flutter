import 'package:flutter/material.dart';
import '../shared/avatar.dart';
import '../contants.dart';

class TransactionRow extends StatelessWidget {
  final String titleName;
  final String time;
  final String desc;
  final String imgPath;
  final double price;
  final double unit;
  final Function onPress;

  TransactionRow(
      {this.titleName,
      this.time,
      this.desc,
      this.imgPath,
      this.price,
      this.unit,
      this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Avatar(
                imgPath: imgPath,
                width: 50,
                height: 50,
                circularWeight: 50,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    titleName,
                    style: kMoneyStyle(),
                  ),
                  Text(
                    time,
                    style: kTitleStyle(),
                  ),
                  Text(
                    desc,
                    style: kTitleStyle(),
                  ),
                ],
              ),
            ),
            Spacer(flex: 4),
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "฿${price.toStringAsFixed(2)}",
                    style: kMoneyStyle(),
                  ),
                  Text(
                    "฿${unit.toStringAsFixed(2)}",
                    style: kTitleStyle(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
