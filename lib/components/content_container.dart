import 'package:flutter/material.dart';

class ContentContainer extends StatelessWidget {
  final Widget body;
  ContentContainer({@required this.body});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 15),
        child: body,
      ),
    );
  }
}
